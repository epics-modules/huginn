#!/usr/bin/env python3
"""
 ------------------------------------------------------------------------------
Unit Test for Huginn Peltier Based Teperature Control
 ------------------------------------------------------------------------------
(c) 2021 European Spallation Source, Lund
 ------------------------------------------------------------------------------
 author: douglas.bezerra.beniz@esss.eu
  ------------------------------------------------------------------------------
"""
import sys, time

import pytest
import epics

# -----------------------------------------------------------------------------
# Declaration of constants and variables
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# EPICS PV:s names constants
# -----------------------------------------------------------------------------
PREFIX          = "UTG-SEE:HUGINN-999"
PV_MODE         = "Mode-S"
PV_SUB_ON       = "SubCryoEnable-S"
PV_SUB_SETP     = "SubCryoTemp-S"
PV_CUR_SETP_OK  = "CurSetpoint_Ok-R"
PV_MAIN_ON      = "MainCryoEnable-S"
PV_MAIN_SETP    = "MainCryoSetpoint-S"
PV_CTR_DELAY    = "CtrDelayOff-R"

# -----------------------------------------------------------------------------
# EPICS PV:s instances
# -----------------------------------------------------------------------------
pvMode      = epics.PV("%s:%s" % (PREFIX, PV_MODE))
pvSubOn     = epics.PV("%s:%s" % (PREFIX, PV_SUB_ON))
pvSubSetP   = epics.PV("%s:%s" % (PREFIX, PV_SUB_SETP))
pvCurSetPOK = epics.PV("%s:%s" % (PREFIX, PV_CUR_SETP_OK))
pvMainOn    = epics.PV("%s:%s" % (PREFIX, PV_MAIN_ON))
pvMainSetP  = epics.PV("%s:%s" % (PREFIX, PV_MAIN_SETP))
pvCtrDelay  = epics.PV("%s:%s" % (PREFIX, PV_CTR_DELAY))


# -----------------------------------------------------------------------------
# Test functions
# -----------------------------------------------------------------------------

@pytest.mark.parametrize('execution_number', range(5))
def test_off_delay(execution_number):
    # -------------------------------------------------------------------------
    # Internal auxiliary functions
    # -------------------------------------------------------------------------
    def __prepareTestEnvironment():
        try:
            # set AUTO mode (2)
            pvMode.put(2)
            # set SubCryo initial temperature
            pvSubOn.put(0)
            pvSubSetP.put(260.0)
            pvSubOn.put(1)
            # set MainCryo initial temperature
            pvMainOn.put(1)
            pvMainSetP.put(300)
        except Exception as e:
            raise e

    # ---------------------------------------------------------------------
    # call function to prepare the test scenario
    # ---------------------------------------------------------------------
    __prepareTestEnvironment()

    # ---------------------------------------------------------------------
    # preamble
    # ---------------------------------------------------------------------
    # wait for temperature within the range
    while pvCurSetPOK.get() != 1:
        time.sleep(1)

    # verify if counter was reset as expected
    assert pvCtrDelay.get() == -1
    # then wait for about 5 seconds to stabilize
    time.sleep(5)

    # ---------------------------------------------------------------------
    # forcing SubCryo out of limits
    # ---------------------------------------------------------------------
    # set MainCryo to a temperature which will cause SuBCryto to go 
    #   outside limits
    pvMainSetP.put(320.0)
    # wait for temperature outside the range
    while pvCurSetPOK.get() != 0:
        time.sleep(2)

    # verify if counter was started as expected
    assert pvCtrDelay.get() > -1
    # then wait for about 5 seconds to stabilize
    time.sleep(5)
