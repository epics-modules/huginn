# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require huginn,     0.0.4
require stream,     2.8.10
require autosave,   5.10.0

# -----------------------------------------------------------------------------
# Simulator
# -----------------------------------------------------------------------------
epicsEnvSet("LOCATION",             "Sim")
epicsEnvSet("SYS",                  "SIM:")
epicsEnvSet("DEV",                  "HUGINN-999")
epicsEnvSet("PREFIX",               "$(SYS)$(DEV)")
epicsEnvSet("IOCNAME",              "SIM-HUGINN-999")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(huginn_DIR)db/")
# -----------------------------------------------------------------------------
# cryostat sub (sample)
#epicsEnvSet("CRYO_SUB",  "UTG-SEE-TEFI:Tctrl-LS336-005")
#epicsEnvSet("CRYO_SUB",  "UTG-SEE:LS336-005")
epicsEnvSet("CRYO_SUB",         "sim_ls03")
epicsEnvSet("T_BASE",           "KRDG2")
epicsEnvSet("T_PELTIER_TOP",    "KRDG2")
epicsEnvSet("T_PELTIER_BOTTOM", "KRDG3")
epicsEnvSet("T_SAMPLE",         "KRDG3")
# -----------------------------------------------------------------------------
# cryostat main
#epicsEnvSet("CRYO_MAIN", "UTG-SEE-TEFI:Tctrl-LS336-002")
#epicsEnvSet("CRYO_MAIN", "UTG-SEE:LS336-002")
epicsEnvSet("CRYO_MAIN",        "sim_ls02")
epicsEnvSet("T_MAIN_BASE",      "KRDG0")
epicsEnvSet("T_MAIN_HEATER",    "KRDG1")
# -----------------------------------------------------------------------------
epicsEnvSet(AS_TOP, "/epics/iocs/autosave/$(IOCNAME)/")

# -----------------------------------------------------------------------------
# Loading databases
# -----------------------------------------------------------------------------
iocshLoad("$(huginn_DIR)huginn.iocsh")
iocshLoad("$(autosave_DIR)autosave.iocsh", "AS_TOP=$(AS_TOP), IOCNAME=$(IOCNAME)")
# -----------------------------------------------------------------------------
# Huginn settings being restored
# -----------------------------------------------------------------------------
# this line will restore data after initialization of array, during pass0 memory for the array is not yet allocated
set_pass1_restoreFile("myArrays.sav", "P=$(PREFIX), CRYO_SUB=$(CRYO_SUB), CRYO_MAIN=$(CRYO_MAIN)")
# Do not create datedBeckupfiles every time when ioc stops. It will create only a backaup file of myArrays.sav named .sav.bu
save_restoreSet_DatedBackupFiles(0)

# -----------------------------------------------------------------------------
# Starting sequencer program
# -----------------------------------------------------------------------------
seq snl_huginn("P=$(PREFIX), CRYO_SUB=$(CRYO_SUB), T_BASE=$(T_BASE)")

# -----------------------------------------------------------------------------
# Creating list of PVs to be used by, e.g., autosave
# -----------------------------------------------------------------------------
# dbl > "$(TOP)/Huginn_PVs.list"
dbl > "${IOCNAME}_PVs.list"

# -----------------------------------------------------------------------------
# Starting the IOC
# -----------------------------------------------------------------------------
iocInit

# Necessary in simulations
dbpf $(PREFIX):#Limits.INPD "$(CRYO_MAIN):KRDG0 CPP"
