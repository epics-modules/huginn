################################################################
### requires
require stream,      2.8.10
require lakeshore336,2.0.5
require iocStats,    3.1.16
require asynfile,    0.0.2

require calc,       3.7.3
require sequencer,  2.2.7

### lakeshore336 (ls1)
epicsEnvSet("LOCATION",             "Utgard-fitlet02:172.30.244.42")

# simulator nr2
#epicsEnvSet("IPADDR",               "172.30.244.42")
epicsEnvSet("IPADDR",               "127.0.0.1")

# real instrument
#epicsEnvSet("IPADDR",               "10.0.7.244")

#epicsEnvSet("SYS",                  "UTG-SIM:")
epicsEnvSet("SYS",                  "sim_")
#epicsEnvSet("DEV",                  "Tmt-LS336-02")
epicsEnvSet("DEV",                  "ls02")
epicsEnvSet("PREFIX",               "$(SYS)$(DEV)")
#epicsEnvSet("IPPORT",               "7777")
epicsEnvSet("IPPORT",               "7772")
epicsEnvSet("IOCNAME",              "LS336-SIM-002")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(lakeshore336_DIR)db/")

### load all db's
iocshLoad("$(lakeshore336_DIR)lakeshore336.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")
iocshLoad("$(iocStats_DIR)/iocStats.iocsh")

### install SNL curves
seq install_curve, "P=$(PREFIX), CurvePrefix=File"

iocInit
