/* -----------------------------------------------------------------------------
 * EPICS module to control Huginn Peltier Based Temperature Control system.
 *
 * -----------------------------------------------------------------------------
 * European Spallation Source ERIC - ICS HWI group, 2018-2021
 * -----------------------------------------------------------------------------
 * Authors : Douglas Bezerra Beniz
 *           Tomasz Brys
 * emails  : douglas.bezerra.beniz@ess.eu
 *           tomasz.brys@ess.eu
 * -----------------------------------------------------------------------------
 */

#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <float.h>


float __interpolate(float refValue, float refTable[], unsigned long nelemRefTable, float tgtTable[], unsigned long nelemTgtTable);
int __isWithinRange(float setPoint, float tBase, float deltaDown, float deltaUp, float safetyFactor);

//=============================================================================
// This function implement linear interpolation 
// For giving arrays values of CalibTemp-S (aCT) and tbase 
// the fuction interpolate a value from arrays DeltaUp-S (aDU) amd DeltaDown-S (aDD)
//
// aCT MUST BE MONOTONIC from max value at the first element to min value at the
// last element as it is done in .sav file, tabY does not have to be monotonic.
// Fuction asumes that number of elements in both tab are the same.
//=============================================================================
static long asub_limits_and_setp(aSubRecord *precord) {
    unsigned long nCT, nDU, nDD;
    float *aCT, *aDU, *aDD;
    float tbase;
    float setPointInput, setPointCur;
    float safetyFactor;
    float up, down;
    int setPointWithinRange;
    int ctrShowMessage, flagClearMessage;
    short huginnMode, huginnModeCur, huginnModeInput, subCryoOn;

    aCT              = (float *)precord->a;    // CalibTemp-S
    nCT              = precord->nea;           // nr of elements in CalibTemp-S
    aDU              = (float *)precord->b;    // DeltaUp-S
    nDU              = precord->neb;           // nr of elements in DeltaUp-S
    aDD              = (float *)precord->c;    // DeltaDown-S
    nDD              = precord->nec;           // nr of elements in DeltaDown-S
    tbase            = *(float*)precord->d;    // tbase from subcryostat 
    setPointCur      = *(float*)precord->e;    // subCryostat current setpoint
    setPointInput    = *(float*)precord->f;    // subCryostat setpoint entered by user
    safetyFactor     = *(float*)precord->g;    // safety factor input
    huginnModeCur    = *(short*)precord->h;    // current operation mode: 0 = "manual"; 1 = "autoMainExternal"; 2 = "autoSub";
    huginnModeInput  = *(short*)precord->i;    // new operation mode;
    subCryoOn        = *(short*)precord->j;    // subCryo ON/OFF, not necessarily LS outputs are ON/OFF too;
    ctrShowMessage   = *(short*)precord->k;    // counter to clean showed message after a while;
    flagClearMessage = *(short*)precord->l;    // flag to indicate if counter has been started;

    //=========================================================================
    // first for DeltaUp-S
    up = __interpolate(tbase, aCT, nCT, aDU, nDU);

    //=========================================================================
    // then for DeltaDown-S
    down = __interpolate(tbase, aCT, nCT, aDD, nDD);

    // ------------------------------------------------------------------------
    // Always update HIGH / LOW limits referencing temperature of BASE; for
    //   safety, TBase outside MIN - MAX in CalibTemp-S is not allowed;
    // ------------------------------------------------------------------------
    if (tbase > aCT[0]) {
        tbase = aCT[0];
    } else if (tbase < aCT[nCT-1]) {
        tbase = aCT[nCT-1];
    }
    // vala => $(P)SubCryoTempHighLimit-R
    // valb => $(P):SubCryoTempLowLimit-R
    *(float *)precord->vala = tbase + up;
    *(float *)precord->valb = tbase - down;
    // valc => $(P):SubCryoTempHighFact-R
    // vald => $(P):SubCryoTempLowFact-R
    *(float *)precord->valc = tbase + up*safetyFactor;
    *(float *)precord->vald = tbase - down*safetyFactor;

    // ------------------------------------------------------------------------
    // Is necessary to update flags regarding input setPoint;
    // ------------------------------------------------------------------------
    // Updates 'within the range' flag for input SetP using 100.0% of margins
    // vali => Setpoint_Ok-R
    setPointWithinRange = __isWithinRange(setPointInput, tbase, down, up, 1.0);
    *(short *)precord->vali = setPointWithinRange;
    // Updates 'within the range' flag for input SetP using SafetyFactor-S
    // valk => SetpointFact_Ok-R
    *(short *)precord->valk = __isWithinRange(setPointInput, tbase, down, up, safetyFactor);

    if ((setPointInput != setPointCur) || (huginnModeInput != huginnModeCur)) {
        // --------------------------------------------------------------------
        // Rules to allow LS Outputs SETP modification depends on:
        //   1 Operation MODE;
        //   2 SubCryo is ON / OFF;
        //   3 New SETP is WITHIN / OUT safe range;
        // --------------------------------------------------------------------
        // Updating Huginn MODE if necessary
        // --------------------------------------------------------------------
        if (huginnModeInput != huginnModeCur) {
            // valg => Mode-RB
            *(short *)precord->valg = huginnModeInput;
            // internal variable
            huginnMode = huginnModeInput;
        } else {
            huginnMode = huginnModeCur;
        }

        // --------------------------------------------------------------------
        // Verifying and Updating SetPoint(s) as necessary
        // --------------------------------------------------------------------
        char message[200];
        // Init string
        memset(message, '\0', sizeof(message));
        // Initialize a COUNTER to clear the message after 5 seconds
        // valm => $(P):#FlagClearMessage
        *(short *)precord->valm = 1;

        // Verifying if should update setpoint in LakeShores or not
        if (((huginnMode == 0) && !subCryoOn) || setPointWithinRange) {
            // ----------------------------------------------------------------
            // SubCryoTemp-S has changed, because it triggers this procedure,
            // ----------------------------------------------------------------
            // AND subCryo is OFF 
            // OR  (subCryo is ON but new SETP is WITHIN the limits);
            //   (if MANUAL mode is selected)
            // ----------------------------------------------------------------
            // AND new SETP is WITHIN the limits;
            //   if (AUTO MAIN-EXTERNAL or AUTO mode is selected)
            // ----------------------------------------------------------------
            // vale => $(P):SubCryoTemp-RB
            *(float *)precord->vale = setPointInput;
            // also updates setPointCur
            setPointCur = setPointInput;
        }

        // Verify if should sync MainCryo setpoint
        if (huginnMode == 2 && subCryoOn) {
            // AUTO mode, also update SETP of Main Cryo if SubCryoEnable-S is "1",
            //   if WITHIN limits or NOT;
            // valf => $(P):MainCryoSetpoint-S
            *(float *)precord->valf = setPointInput;
        }

        // Verifying which situation we have to notify the operator
        if (setPointWithinRange) {
            strcat(message, "SetPoint within the limits");
        } else if (!subCryoOn && !setPointWithinRange) {
            strcat(message, "SetPoint outside the limits");
        } else if (!setPointWithinRange) {
            strcat(message, "SetPoint not updated; out of range");
        } else {
            strcat(message, "");
        }

        // Update WARNING message to user
        // vall => $(P):WarningAsub-R
        strcpy(precord->vall, message);
    } else if (flagClearMessage && ctrShowMessage <= 0) {
        // clear message
        char message[200];
        memset(message, '\0', sizeof(message));
        strcat(message, "");
        // vall => $(P):WarningAsub-R
        strcpy(precord->vall, message);
        // Reset ShowMessage FLAG
        // valm => $(P):#FlagClearMessage
        *(short *)precord->valm = 0;
    }

    // ------------------------------------------------------------------------
    // This is used to verify if current setPoint is out.of-range (for any
    //   reason then state-machine should force SubCryo output to OFF;
    // It is necessary to check after validations because setPointCur may
    //   have been modified due them;
    // ------------------------------------------------------------------------
    // Updates 'within the range' flag for current SetP using 100.0% of margins
    // valh => CurSetpoint_Ok-R
    *(short *)precord->valh = __isWithinRange(setPointCur, tbase, down, up, 1.0);
    // Updates 'within the range' flag for current SetP using SafetyFactor
    // valj => CurSetpointFact_Ok-R
    *(short *)precord->valj = __isWithinRange(setPointCur, tbase, down, up, safetyFactor);

    return 0;
}


//=============================================================================
// Verification if Peltiers (TOP, BOTTOM) are inside desired range, considering
//   a tolerance;
// Runs each 1 second (SCAN in database); that is why is used by all COUNTERS;
//=============================================================================
static long asub_ctrs_clock(aSubRecord *precord) {
    float tp1             = *(float*)precord->a;  // $(CRYO_SUB):$(T_PELTIER_TOP)
    float tp2             = *(float*)precord->b;  // $(CRYO_SUB):$(T_PELTIER_BOTTOM)
    float setpoint        = *(float*)precord->c;  // $(P):SubCryoTemp-RB
    float tolerance       = *(float*)precord->d;  // $(P):Tolerance-S
    int toleranceTime     = *(short*)precord->e;  // $(P):ToleranceTime-S
    int ctrInRange        = *(short*)precord->f;  // $(P):CtrInRange-R
    int ctrShowMessage    = *(short*)precord->g;  // $(P):CtrShowMessage-R
    int ctrShowMessageSeq = *(short*)precord->h;  // $(P):CtrShowMessageSeq-R
    int ctrDelayOff       = *(short*)precord->i;  // $(P):CtrDelayOff-R
    int delayOff          = *(short*)precord->j;  // $(P):DelayOff
    int flagClearMessage  = *(short*)precord->k;  // $(P):#FlagClearMessage
    int flagDelayOff      = *(short*)precord->l;  // $(P):#FlagDelayOff
    int inrange;

    if ((abs(tp1 - setpoint) < tolerance ) && (abs(tp2 - setpoint) < tolerance)) {
        inrange = 1;
    } else {
        inrange = 0;
    }

    // ------------------------------------------------------------------------
    // vala => ReadyToMeasure-R
    // valb => CtrInRange
    // ------------------------------------------------------------------------
    if (inrange && ctrInRange > 0) {
        ctrInRange--;
        *(short *)precord->vala = 0;
        *(short *)precord->valb = ctrInRange;
    } else if (inrange && ctrInRange <= 0) {
        *(short *)precord->vala = 1;
        *(short *)precord->valb = 0;
    } else {
        *(short *)precord->vala = 0;
        *(short *)precord->valb = toleranceTime;
    }

    // ------------------------------------------------------------------------
    // Updates ShowMessage COUNTER (each 1 s.) if has been started a countdown;
    // ------------------------------------------------------------------------
    if (flagClearMessage && ctrShowMessage > 0) {
        ctrShowMessage--;
        // valc => $(P):CtrShowMessage-R
        *(short *)precord->valc = ctrShowMessage;
    } else if (!flagClearMessage) {
        // Keep ShowMessage FLAG in 5 seconds normally;
        // valc => $(P):CtrShowMessage-R
        *(short *)precord->valc = 5;
    }

    // ------------------------------------------------------------------------
    // Updates ShowMessageSeq COUNTER (each 1 s.) if has been started a
    // countdown; used in Sequencer SNL program to clear Warning messages;
    // ------------------------------------------------------------------------
    if (ctrShowMessageSeq > 0) {
        ctrShowMessageSeq--;
        // vald => $(P):CtrShowMessageSeq-R
        *(short *)precord->vald = ctrShowMessageSeq;
    }

    // ------------------------------------------------------------------------
    // Updates DelayOff COUNTER (each 1 s.) if has been started a countdown;
    // ------------------------------------------------------------------------
    if (ctrDelayOff > -1) {
        // First, verify if DelayOff flag was not reset
        if (!flagDelayOff) {
            // vale => $(P):CtrDelayOff-R
            *(short *)precord->vale = -1;
        } else {
            // It may happen that operator changes DelayOff-S during count
            if (ctrDelayOff > delayOff) {
                ctrDelayOff = delayOff;
            } else {
                ctrDelayOff--;
            }
            // vale => $(P):CtrDelayOff-R
            *(short *)precord->vale = ctrDelayOff;
        }
    } else if (flagDelayOff) {
        // If DelayOff FLAG has been set, initializes the counter
        // vale => $(P):CtrDelayOff-R
        *(short *)precord->vale = delayOff;
    }

    return 0;
}


//=============================================================================
// This function implement linear interpolation 
// For received arrays values of PID_Temperatures (aPID_Temp) and BASE temperature (tbase)
// the fuction interpolate a value from arrays PID_P (aPID_P), PID_I (aPID_I)
// and PID_D (aPID_D).
//
// aPID_Temp MUST BE MONOTONIC from max value at the first element to min value at the
// last element as it is done in .sav file, tabY does not have to be monotonic.
//
// Fuction asumes that number of elements in both tab are the same.
//=============================================================================
static long asub_update_pid(aSubRecord *precord) {
    unsigned long nPID_Temp, nPID_P, nPID_I, nPID_D;
    float *aPID_Temp, *aPID_P, *aPID_I, *aPID_D;
    float tbase;
    float new_pid[3];

    aPID_Temp        = (float *)precord->a;    // PID_T (temperatures) table
    nPID_Temp        = precord->nea;           // nr of elements in PID_T
    aPID_P           = (float *)precord->b;    // PID_P (proportional) table
    nPID_P           = precord->neb;           // nr of elements in PID_P
    aPID_I           = (float *)precord->c;    // PID_I (integral) table
    nPID_I           = precord->nec;           // nr of elements in PID_I
    aPID_D           = (float *)precord->d;    // PID_D (derivative) table
    nPID_D           = precord->ned;           // nr of elements in PID_D
    tbase            = *(float*)precord->e;    // tbase from subcryostat 

    //=========================================================================
    // first for PID_P (proportional)
    new_pid[0] = __interpolate(tbase, aPID_Temp, nPID_Temp, aPID_P, nPID_P);

    //=========================================================================
    // thenr PID_I (integral)
    new_pid[1] = __interpolate(tbase, aPID_Temp, nPID_Temp, aPID_I, nPID_I);

    //=========================================================================
    // then PID_D (derivative)
    new_pid[2] = __interpolate(tbase, aPID_Temp, nPID_Temp, aPID_D, nPID_D);

    // ------------------------------------------------------------------------
    // Update P.I.D.s of analog outputs referencing temperature of BASE
    // ------------------------------------------------------------------------
    // vala => $(P):#SubAOutPID3_Oper
    // valb => $(P):#SubAOutPID4_Oper
    memcpy(precord->vala, new_pid, 3*sizeof(float));
    memcpy(precord->valb, new_pid, 3*sizeof(float));

    return 0;
}


//=============================================================================
// Functions for internal usage
//=============================================================================
float __interpolate(float refValue, float refTable[], unsigned long nelemRefTable, float tgtTable[], unsigned long nelemTgtTable) {
    // instantiating necessary variables
    float delta = FLT_MAX; // maximum finite value of float;
    float newDelta;
    float dx, dy;
    float interpolatedValue = 0.0;
    int   i, idx;

    if (refValue >= refTable[0]) {
        interpolatedValue = tgtTable[0];
    } else if (refValue <= refTable[nelemRefTable-1]) {
        interpolatedValue = tgtTable[nelemTgtTable-1];
    } else {
        // Finds nearest neighbour index in the reference table
        idx = -1;
        for (i = 0; i < nelemRefTable; i++) {
            newDelta = refValue - refTable[i];
            if (newDelta >= 0 && newDelta < delta) {
                delta = newDelta;
                idx   = i;
            }
        }
        // Calculates interpolation using dx and dy based on the Index of
        //   references in configured tables
        dx = refTable[idx-1] - refTable[idx];
        if (dx == 0) dx = 1;
        dy = tgtTable[idx-1] - tgtTable[idx];
        // calculation
        interpolatedValue = tgtTable[idx] + (refValue - refTable[idx]) * dy / dx; 
    }

    return interpolatedValue;
}

//=============================================================================
// Returns when the SetPoint is within / out the safe range
//=============================================================================
int __isWithinRange(float setPoint, float tBase, float deltaDown, float deltaUp, float safetyFactor) {
    int isWithinRange;

    if (tBase == 0.0) {
        return 0;
    }

    if ((setPoint > (tBase - deltaDown*safetyFactor)) && (setPoint < (tBase + deltaUp*safetyFactor))) {
        isWithinRange = 1;
    } else {
        isWithinRange = 0;
    }

    return isWithinRange;
}

//=============================================================================
// Registering functions to be used by EPICS
//=============================================================================
epicsRegisterFunction(asub_limits_and_setp);
epicsRegisterFunction(asub_ctrs_clock);
epicsRegisterFunction(asub_update_pid);
