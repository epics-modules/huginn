#!/bin/bash

# -----------------------------------------------------------------------------
# ESS
# -----------------------------------------------------------------------------
# Huginn
#
# Author: douglas.bezerra.beniz@ess.eu
# -----------------------------------------------------------------------------
# input params
AUTOSAVE_DIR="$1"
AUTOSAVE_FILE="$2"

# -----------------------------------------------------------------------------
# copying a saved settings file necessary for huginn operation after verify the
# folder exists
# -----------------------------------------------------------------------------
# creating the directory if does not exists
mkdir -p $AUTOSAVE_DIR
# copying .sav file (if any)
if [ -f "$AUTOSAVE_FILE" ]; then
    # echo "$AUTOSAVE_FILE exists."
    cp $AUTOSAVE_FILE $AUTOSAVE_DIR
fi
