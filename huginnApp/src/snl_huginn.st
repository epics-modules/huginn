/* -----------------------------------------------------------------------------
 * EPICS module to control Huginn Peltier Based Temperature Control system.
 *
 * -----------------------------------------------------------------------------
 * European Spallation Source ERIC - ICS HWI group, 2018-2021
 * -----------------------------------------------------------------------------
 * Authors : Douglas Bezerra Beniz
 *           Tomasz Brys
 * emails  : douglas.bezerra.beniz@ess.eu
 *           tomasz.brys@ess.eu
 * -----------------------------------------------------------------------------
 */

program snl_huginn

%%#include <stdio.h>
%%#include <stdlib.h>
%%#include <errlog.h>
%%#include <time.h>

/* ----------------------------------------------------------------------------
 * General use variables
 * ------------------------------------------------------------------------- */
int SHOW_TIME = 5;  // seconds to keep showing messages in OPI

/* ----------------------------------------------------------------------------
 * Channels assigned to records
 * ------------------------------------------------------------------------- */

/* 
 * (current) huginn operation mode:
 *   0 :: Manual
 *   1 :: Auto Main-External
 *   2 :: Auto
 */
int huginnMode;
assign  huginnMode to "{P}:Mode-RB";
monitor huginnMode;

// subCryostat current setpoint
float subSetPSet;
assign  subSetPSet to "{P}:SubCryoTemp-RB";
monitor subSetPSet;

// subCryostat setpoint entered by user
float subSetPInp;
assign  subSetPInp to "{P}:SubCryoTemp-S";
monitor subSetPInp;

// subCryo ON/OFF write (however monitored because is an operator:s input)
int subOn;
assign  subOn to "{P}:SubCryoEnable-S";
monitor subOn;

// LS (Peltier-1) write
int subOn_S3;
assign subOn_S3 to "{P}:SubCryoEnable_Out3-S";

// LS (Peltier-1) read
int subOn_S3_rbv;
assign  subOn_S3_rbv to "{P}:SubCryoEnable_Out3-RB";
monitor subOn_S3_rbv;

// LS (Peltier-2) write
int subOn_S4;
assign subOn_S4 to "{P}:SubCryoEnable_Out4-S";

// LS (Peltier-2) read
int subOn_S4_rbv;
assign  subOn_S4_rbv to "{P}:SubCryoEnable_Out4-RB";
monitor subOn_S4_rbv;

// base temperature used by SubCryo as reference
float subCryoTBase;
assign  subCryoTBase to "{CRYO_SUB}:{T_BASE}";
monitor subCryoTBase;

// simply enables/disables mainCryo ON/OFF input
int mainEnable;
assign mainEnable to "{P}:MainUnlocked-R";

// mainCryo ON/OFF write
int mainOn;
assign mainOn to "{P}:MainCryoEnable-S";

// mainCryo ON/OFF read
int mainOn_rbv;
assign  mainOn_rbv to "{P}:MainCryoEnable-RB";
monitor mainOn_rbv;

/*
 * flags updated by 'asub_limits_and_setp()' function
 */
// when new SetPoint is within the range without SafetyFactor-S
int isSetPointOK;
assign  isSetPointOK to "{P}:Setpoint_Ok-R";
monitor isSetPointOK;

// when current SetPoint is within the range without SafetyFactor-S
int isCurSetPointOK;
assign  isCurSetPointOK to "{P}:CurSetpoint_Ok-R";
monitor isCurSetPointOK;

// when new SetPoint is within the range with SafetyFactor-S
int isSetPFactorOK;
assign  isSetPFactorOK to "{P}:SetpointFact_Ok-R";
monitor isSetPFactorOK;

// when current SetPoint is within the range with SafetyFactor-S
int isCurSetPFactorOK;
assign  isCurSetPFactorOK to "{P}:CurSetpointFact_Ok-R";
monitor isCurSetPFactorOK;

// when should start updating related counter
int flagDelayOff;
assign  flagDelayOff to "{P}:#FlagDelayOff";

// counter to manage when delay before switch peltiers OFF must be observed
int ctrDelayOff;
assign  ctrDelayOff to "{P}:CtrDelayOff-R";
monitor ctrDelayOff;
// event flag in sync with a monitored channel to prevent weird behaviour
evflag  ctrDelayOffFlag;
sync    ctrDelayOff ctrDelayOffFlag;

// counter to manage when messages to operator must be cleared
int ctrShowMessageSeq;
assign  ctrShowMessageSeq to "{P}:CtrShowMessageSeq-R";
monitor ctrShowMessageSeq;
// event flag in sync with a monitored channel to prevent weird behaviour
evflag  ctrShowMessageSeqFlag;
sync    ctrShowMessageSeq ctrShowMessageSeqFlag;

/* end of 'asub_limits_and_setp' flags */

// messages to operator
string message;
assign message to "{P}:Message-R";

string warning;
assign warning to "{P}:WarningSeq-R";

//option +r;
//option +s;

ss peltiersControl {
    //-------------------------------------------------------------------------
    // INIT
    //-------------------------------------------------------------------------
    state init {
        entry {
            // Wait for database loading...
            epicsThreadSleep(1.0);
            // Logging the start
            strcpy(message,"Starting Huginn ...");
            pvPut(message, SYNC);
        }

        when(subOn_S3_rbv || subOn_S4_rbv) {
        } state subCryoIsOn

        when(!subOn_S3_rbv && !subOn_S4_rbv) {
        } state subCryoIsOff
    }

    //-------------------------------------------------------------------------
    // SUB-CRYO_SET_OFF
    //-------------------------------------------------------------------------
    state subCryoSetOff {
        /* Switching SubCryo OFF */
        when() {
            subOn_S3 = 0;
            pvPut(subOn_S3, SYNC);
            subOn_S4 = 0;
            pvPut(subOn_S4, SYNC);
        } state subCryoIsOff
    }

    //-------------------------------------------------------------------------
    // SUB-CRYO_IS_OFF
    //-------------------------------------------------------------------------
    state subCryoIsOff {
        /* Operator switched ON SubCryo, but depends on some conditions */
        when(subOn && (huginnMode == 0) && isSetPointOK && isCurSetPointOK) {
            // MANUAL mode
            // Logging action
            sprintf(message, "Mode: %d, switching Peltiers On", huginnMode);
            pvPut(message, SYNC);
            // Informs operator
            strcpy(warning, "Switching Peltiers On");
            pvPut(warning, SYNC);
            // Start counting to then clear the message
            ctrShowMessageSeq = SHOW_TIME;
            pvPut(ctrShowMessageSeq, SYNC);
        } state subCryoSetOn

        /* Operator switched ON SubCryo, but depends on some conditions */
        when(subOn && (huginnMode == 1 || huginnMode == 2) && isSetPFactorOK && isCurSetPFactorOK) {
            // AUTO or MAIN-EXTERNAL mode
            // Logging action
            sprintf(message, "Mode: %d, turn Peltiers On SafeFact", huginnMode);
            pvPut(message, SYNC);
            // Informs operator
            strcpy(warning, "Switching Peltiers On, used SafeFactor");
            pvPut(warning, SYNC);
            // Start counting to then clear the message
            ctrShowMessageSeq = SHOW_TIME;
            pvPut(ctrShowMessageSeq, SYNC);
        } state subCryoSetOn

        /* Verify if a counter to show messages was upated, if reached zero
         *   clear the message */
        when(efTestAndClear(ctrShowMessageSeqFlag) && ctrShowMessageSeq <= 0) {
            strcpy(warning, "");
            pvPut(warning, SYNC);
        } state subCryoIsOff
    }

    //-------------------------------------------------------------------------
    // SUB-CRYO_SET_ON
    //-------------------------------------------------------------------------
    state subCryoSetOn {
        /* Switching SubCryo ON */
        when() {
            subOn_S3 = 1;
            pvPut(subOn_S3, SYNC);
            subOn_S4 = 1;
            pvPut(subOn_S4, SYNC);
        } state subCryoIsOn
    }

    //-------------------------------------------------------------------------
    // SUB-CRYO_IS_ON
    //-------------------------------------------------------------------------
    state subCryoIsOn {
        /* Operator switched OFF SubCryo, just do it */
        when(!subOn) {
        } state subCryoSetOff

        /* TBase is ZERO; maybe sensor is unplugged*/
        when(subCryoTBase == 0.0) {
            // Logging action after obtains the current mode
            pvGet(huginnMode);
            sprintf(message, "Mode: %d, TBase == 0; switching Off", huginnMode);
            pvPut(message, SYNC);
            // Informs operator
            strcpy(warning, "Switching Peltiers Off; TBase is zero");
            pvPut(warning, SYNC);
            // Start counting to then clear the message
            ctrShowMessageSeq = SHOW_TIME;
            pvPut(ctrShowMessageSeq, SYNC);
        } state subCryoSetOff

        /* When SetPoint has fallen outside DeltaTemp-R limits switch OFF SubCryo */
        when((huginnMode == 0) && !isCurSetPointOK) {
            // MANUAL mode
            // Logging action
            sprintf(message, "Mode: %d, switching Peltiers Off", huginnMode);
            pvPut(message, SYNC);
            // Informs operator
            strcpy(warning, "Switching Peltiers Off; out of range");
            pvPut(warning, SYNC);
            // Start counting to then clear the message
            ctrShowMessageSeq = SHOW_TIME;
            pvPut(ctrShowMessageSeq, SYNC);
        } state subCryoSetOff

        /* When AUTO modes and a NEW SetPoint is outside DeltaTemp-R limits switch
             OFF SubCryo */
        when((huginnMode == 1 || huginnMode == 2) && (subSetPInp != subSetPSet) && !isSetPointOK) {
            // MANUAL mode
            // Logging action
            sprintf(message, "Mode: %d, switching Peltiers Off", huginnMode);
            pvPut(message, SYNC);
            // Informs operator
            strcpy(warning, "Switching Peltiers Off; out of range");
            pvPut(warning, SYNC);
            // Start counting to then clear the message
            ctrShowMessageSeq = SHOW_TIME;
            pvPut(ctrShowMessageSeq, SYNC);
        } state subCryoSetOff

        /* When SetPoint has fallen outside DeltaTemp-R limits switch OFF SubCryo */
        when(efTestAndClear(ctrDelayOffFlag) && ctrDelayOff <= 0 && (!isSetPointOK || !isCurSetPointOK)) {
            // AUTO or MAIN-EXTERNAL mode
            // ----------------------------------------------------------------
            // In AUTO modes this may have happened due to changes in BASE
            //   temperature; a configured OFF_DELAY must be observed before
            //   switch Peltiers OFF;
            // ----------------------------------------------------------------
            // Logging action
            sprintf(message, "Mode: %d, switching Peltiers Off", huginnMode);
            pvPut(message, ASYNC);
            // Informs operator
            strcpy(warning, "Switch Peltiers Off; end of countdown");
            pvPut(warning, SYNC);
            // Start counting to then clear the message
            ctrShowMessageSeq = SHOW_TIME;
            pvPut(ctrShowMessageSeq, SYNC);
        } state subCryoSetOff

        /* Verify if a counter to show messages was upated, if reached zero
         *   clear the message */
        when(efTestAndClear(ctrShowMessageSeqFlag) && ctrShowMessageSeq <= 0) {
            strcpy(warning, "");
            pvPut(warning, SYNC);
        } state subCryoIsOn
    }
}

ss mainCryoControl {
    //-------------------------------------------------------------------------
    // INIT
    //-------------------------------------------------------------------------
    state init {
        when(mainOn_rbv) {
            // Keeping consistency, sets the ON/OFF input with readback
            mainOn = 1;
            pvPut(mainOn, SYNC);
            // From database is known that MainEnable starts with 1
        } state mainCryoIsEnabled

        when(!mainOn_rbv) {
            // Keeping consistency, sets the ON/OFF input with readback
            mainOn = 0;
            pvPut(mainOn, SYNC);
            // From database is known that MainEnable starts with 1
        } state mainCryoIsEnabled
    }

    //-------------------------------------------------------------------------
    // MAIN-CRYO_IS_ENABLED
    //-------------------------------------------------------------------------
    state mainCryoIsEnabled {
        /* Auto mode does not allow operators to control MainCryo, always ON */
        when(huginnMode == 2) {
            // Mode switched to Auto, then disables the control
            mainEnable = 0;
            pvPut(mainEnable, SYNC);
            // And forces MainCryoOn to ON
            mainOn = 1;
            pvPut(mainOn, SYNC);
        } state mainCryoIsDisabled
    }

    //-------------------------------------------------------------------------
    // MAIN-CRYO_IS_DISABLED
    //-------------------------------------------------------------------------
    state mainCryoIsDisabled {
        /* Manual and Auto Main-External modes allow operators to control
         *   MainCryo */
        when(huginnMode == 0 || huginnMode == 1) {
            // Switched to Manual or Auto Main-External, enables the control
            mainEnable = 1;
            pvPut(mainEnable, SYNC);
        } state mainCryoIsEnabled
    }
}

ss delayOffControl {
    //-------------------------------------------------------------------------
    // DELAY_OFF_STANDBY
    //-------------------------------------------------------------------------
    state delayOffStandby {
        /* Auto modes may require OFF Delay countdown */
        when((huginnMode == 1 || huginnMode == 2) && (subOn_S3_rbv || subOn_S4_rbv) && !isCurSetPointOK) {
            /* Set DelayOff flag to start the countdown once SetPoint is
             *   outside limits */
            flagDelayOff = 1;
            pvPut(flagDelayOff, SYNC);
        } state delayOffCounting
    }

    //-------------------------------------------------------------------------
    // DELAY_OFF_COUNTING
    //-------------------------------------------------------------------------
    state delayOffCounting {
        /* If OFF Delay countdown has been started verify if needs to update it */
        when(efTest(ctrDelayOffFlag) && isCurSetPointOK && (subOn_S3_rbv || subOn_S4_rbv)) {
            /* Counter reached the end but SetPoint has been stabilized, only
             *   log what happened and reset the flag; */
            flagDelayOff = 0;
            pvPut(flagDelayOff, SYNC);
            sprintf(message, "Back to range in OffDelay countdown", huginnMode);
            pvPut(message, SYNC);
        } state delayOffStandby

        when(efTest(ctrDelayOffFlag) && ctrDelayOff <= 0 && !subOn_S3_rbv && !subOn_S4_rbv) {
            /* Counter reached the end and didn't stabilize, 'peltiersControl'
             *   StatesSet must have switched Peltiers OFF; reset the flag; */
            flagDelayOff = 0;
            pvPut(flagDelayOff, SYNC);
            sprintf(message, "End of OffDelay countdown", huginnMode);
            pvPut(message, ASYNC);
        } state delayOffStandby

        when(!subOn) {
            /* Operator switched OFF SubCryo, just do it */
            flagDelayOff = 0;
            pvPut(flagDelayOff, SYNC);
            sprintf(message, "Cancelled OffDelay countdown", huginnMode);
            pvPut(message, ASYNC);
        } state delayOffStandby
    }
}
